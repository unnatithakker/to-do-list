import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { ToDoComponent } from './to-do/to-do.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: LoginComponent
    }, {
        path: 'todolist',
        component: ToDoComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
