import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../app.service';

@Component({
  selector: 'todo-list',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})

export class ToDoComponent implements OnInit {
    public newTodo: string;
    public todos: any;
    public todoObj: any;

    constructor(private router: Router, public _appService : AppService) {
      this.newTodo = '';
      console.log('in to do')
      this.todos = [];
    }

    ngOnInit(){
        if(!this._appService.isAuthenticated()){
            this.logout();
        }
    }

    addTodo(event) {
      this.todoObj = {
        newTodo: this.newTodo,
        completed: false
      }
      this.todos.push(this.todoObj);
      this.newTodo = '';
      event.preventDefault();
    }

    deleteTodo(index) {
      this.todos.splice(index, 1);
    }

    deleteSelectedTodos() {
      //need ES5 to reverse loop in order to splice by index
      for(var i=(this.todos.length -1); i > -1; i--) {
        if(this.todos[i].completed) {
          this.todos.splice(i, 1);
        }
      }
    }

    logout(){
        this.router.navigate(['/']);
    }

    selectAll(){
      for(var i= 0; i < this.todos.length; i++) {
        this.todos[i].completed = true;
      }
    }
}