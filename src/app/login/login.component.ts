import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../app.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
    public username;
    public password;
    constructor(private router: Router, public _appService : AppService){}
    login(){
        if(this.username =='admin' && this.password=='admin'){
            this._appService.setUser(this.username, this.password);
             this.router.navigate(['todolist']);
        }
    }
}